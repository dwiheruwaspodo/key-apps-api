// cara lama nggak pake es6
// const express =  require('express');

import express from 'express';
import config from './configs';
import bodyParser from 'body-parser';

// routes
import { routeUser } from './src/modules/Users/UserRouter';
import { routeKeypass } from './src/modules/KeyPass/KeyPassRoute';

const app = express();
// untuk menerima post json
app.use(bodyParser.json());
// untuk menerima post urlencode
app.use(bodyParser.urlencoded({extended: true}));

// router assign
app.use('/api', routeUser)
app.use('/api', routeKeypass)

app.listen(config.port, function() {
	console.log('listening on port' + config.port);
});