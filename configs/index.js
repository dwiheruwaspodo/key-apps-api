'use strict';
// nggak bisa pake export / es6
// pake module.export
module.exports = {
	port: 7777,
	development : {
		client: 'mysql',
		connection: {
			host    : '127.0.0.1',
			user    : 'root',
			password: 'root',
			database: 'project_keypass',
			charset : 'utf8'
		},
	  	migrations: {
	    	tableName: 'knex_migrations'
	  	}
	},
	staging: {
	  	client: 'mysql',
		connection: {
			host    : '127.0.0.1',
			user    : 'root',
			password: 'root',
			database: 'project_keypass',
			charset : 'utf8'
		},
	  	pool: {
	    	min: 2,
	    	max: 10
	  	},
	  	migrations: {
	    tableName: 'knex_migrations'
	  }
	},
	production: {
	  	client: 'mysql',
		connection: {
			host    : '127.0.0.1',
			user    : 'root',
			password: 'root',
			database: 'project_keypass',
			charset : 'utf8'
		},
	  	pool: {
	    	min: 2,
	    	max: 10
	  	},
	  	migrations: {
	    	tableName: 'knex_migrations'
	  	}
	},
	email: {
        host: "smtp.mailtrap.io",
		port: 2525,
		auth: {
			user: "86a030d553f09e",
			pass: "d2927d91110337"
		}
    }
}