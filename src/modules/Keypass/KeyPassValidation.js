import Validator from "validatorjs";

export const validateCreate = async (data) => {
	let rules = {
		key      : 'required',
		username : 'required',
		password : 'required',
	};

	let message = {
		"required.key"      : 'key wajib diisi, cuk!',
		"required.username" : 'username wajib diisi, cuk!',
		"required.password" : 'password wajib disini, cuk!'
	}

	return new Validator(data, rules, message);
}

export const validateUpdate = async (data) => {
	let rules = {
		id       : 'required',
		key      : 'max:100',
		username : 'max:100',
		password : 'max:200',
	};

	let message = {
		"required.id"      : 'id wajib diisi, cuk!',
	}

	return new Validator(data, rules, message);
}

export const validateDelete = async (data) => {
	let rules = {
		id       : 'required'
	};

	let message = {
		"required.id"      : 'id wajib diisi, cuk!',
	}

	return new Validator(data, rules, message);
}

