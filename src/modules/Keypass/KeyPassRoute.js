import express from 'express';
import auth from '../../middleware/auth';
import { create, update, deleted } from './KeyPassController';

export const routeKeypass = express.Router();

routeKeypass.post('/keypass/create', auth(), create);
routeKeypass.post('/keypass/update', auth(), update);
routeKeypass.post('/keypass/delete', auth(), deleted);
