import express from 'express';
import { postLoginJwt, infoJwt, myPass, firstOrCreateUser, emailVerify } from './UserController';
import auth from '../../middleware/auth';

export const routeUser = express.Router();

routeUser.post('/login', postLoginJwt);
routeUser.post('/check-user', firstOrCreateUser);
routeUser.get('/me', auth(), infoJwt);
routeUser.get('/my-pass', auth(), myPass);
routeUser.get('/verify-email/:email/:code', emailVerify);