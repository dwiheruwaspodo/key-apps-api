import Validator from "validatorjs";

export const validateLogin = async (data) => {
	let rules = {
		email    : 'required|email',
		password : 'required',
	};

	let message = {
		"required.email"    : 'email wajib diisi, cuk!',
		"email.email"       : 'kudu email, cuk!',
		"required.password" : 'password wajib disini, cuk!'
	}

	return new Validator(data, rules, message);
}

export const validateFirstOrCreate = async (data) => {
	let rules = {
		email    : 'required|email'
	};

	let message = {
		"required.email"    : 'email wajib diisi, cuk!',
		"email.email"       : 'kudu email, cuk!'
	}

	return new Validator(data, rules, message);
}

