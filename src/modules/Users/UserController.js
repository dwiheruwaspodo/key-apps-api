'use strict'
import express from 'express';
import core from "../../core";
import bcrypt from 'bcryptjs';
import jwt from "jsonwebtoken";
import nodemailer from 'nodemailer';
import {UserModel} from "./UserModel";
import { validateLogin, validateFirstOrCreate } from "./UserValidation";
import configs from '../../../configs';
import edge from 'edge.js';
import path from "path";

const baseUrl   = express()
const returnApi = core.returnApi
const randomStr = core.createRandom
const sendEmail = core.sendEmail
const bookshelf = core.database

export const postLoginJwt = async (req, res) => {
	let data       = req.body;
	let validation = await validateLogin(data)

	if (validation.fails()) {
		return res.json(validation.errors.all())
	}

	let check = await checkUserByMail(data.email)

	if (check) {
		check = check.toJSON()
		if (bcrypt.compareSync(data.password, check.password)) {
			let token = {
				token  : jwt.sign(check, core.jwtToken, { expiresIn: 60 * 60})
			}

			return res.json(token)
		}
	}

	return res.json(returnApi({}))
}

export const firstOrCreateUser = async (req, res) => {
	let data       = req.body	
	let validation = await validateFirstOrCreate(data)
	let flag       = 0
	let sendMail   = {}

	if (validation.fails()) {
		return res.json(validation.errors.all())
	}

	let genCode = await generateCode(data.email)
	let check   = await checkUserByMail(data.email)
	let baseurl = req.headers.host

    if (check === null) {
    	let createnew = await createUserByMail(data.email)

    	if (createnew === null) {
    		return res.json(returnApi(false))
    	}

		flag  = 1
		check = createnew
    }

    if (!checkVerifyEmail(check) && flag === 0) {
    	return res.json({
			status   : "fail",
			messages : ["verifikasi email first!"]
    	})
    }

    if (flag == 1) {
    	sendMail = await sendEmail(data.email, "Verify", templateEmailVerify(baseurl, check))
    } else {
		sendMail = await sendEmail(data.email, "Code", templateEmailCode(check))
    }

	return res.json({
		status : "success",
		flag   : flag,
		mail   : sendMail
	})
}

const checkVerifyEmail = (data) => {
	data = data.toJSON()

	if (data.email_verify !== null) {
		return false
	}

	return true;
}

const checkUserByMail = (mail) => {
	return UserModel.forge().where({email: mail}).fetch()
}

const createUserByMail = async (mail) => {
	let forPass = randomStr(core.digitCode)
	let data = {
		'username'     : mail,
		'email'        : mail,
		'email_verify' : randomStr(core.digitVerify),
		'password'     : bcrypt.hashSync(forPass, core.saltRounds),
		'code'         : forPass
	}

	let save = await bookshelf.default.transaction(function (transaction) {
				UserModel.forge()
					.save(data, {transacting: transaction})
				    .then(transaction.commit)
				    .catch(transaction.rollback);
				}).asCallback();

	return save;
}

const generateCode = async (mail) => {
	let forPass = randomStr(core.digitCode)
	let data = {
		'password' : bcrypt.hashSync(forPass, core.saltRounds),
		'code'     : forPass
	}

	let save = await UserModel
					.where({email: mail})
					.save(data,  {patch:true, method: 'update', require: false})
	return save;
}

const templateEmailVerify = (baseurl, data) => {
	data = data.toJSON()

	edge.registerViews(path.join(__dirname, './Mails'))

	const mail = {
	  url_verification: "http://"+baseurl+'/api/verify-email/'+core.base64encode(data.email)+'/'+core.base64encode(data.email_verify)
	}

	return edge.render('email_verify', mail)
}

const templateEmailCode = (data) => {
	data = data.toJSON()

	edge.registerViews(path.join(__dirname, './Mails'))

	const mail = {
	  code: data.code
	}

	return edge.render('email_code', mail)
}

const templateYay = () => {
	edge.registerViews(path.join(__dirname, './Mails'))
	return edge.render('yay')
}

export const infoJwt = async (req, res) => {
	return res.json({user: req.user})
}

export const myPass = async (req, res) => {
	let pass = await UserModel
				.forge()
				.where({id: req.user.id})
				.fetch({withRelated: ['keyPass']})
	if (pass) {
		pass = pass.toJSON()
		return res.json(returnApi(pass))
	}

	return res.json(returnApi(false))
}

export const emailVerify = async (req, res) => {
	let { email, code } = req.params
	email               = core.base64decode(email)
	code                = core.base64decode(code)
	
	let user            = await UserModel.where({email: email, email_verify: code})
	let check           = await user.fetch()

	if (check !== null) {
		let save = await user.save({'email_verify': null}, {patch:true, method: 'update', require: false})
		
		if (save) {
			return res.send(templateYay())
		}
	}

	return res.redirect('http://google.com');
}