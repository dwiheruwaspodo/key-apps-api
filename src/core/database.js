import knex from 'knex';
import config from '../../configs';
import bookshelf from 'bookshelf';

const db = bookshelf(knex(config.development));
export default db ;


/* KNEX seed - nggak bisa di ES6 */
// const knex      = require('knex')
// const config    = require('../../configs')
// const bookshelf = require('bookshelf')

// const db = bookshelf(knex(config.development));


// module.exports = { db }